package com.lendenclub.test;

import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

public class AdharCard {
	WaitForTime time = new WaitForTime();

	public void page007AdharCard() {

		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.TextView"))
		.click(); // Click Front image of Aadhar Card to upload
		BorrowerRegistration.driver.findElement(By.id("android:id/button2")).click(); // Select Gallery option
		// Click Personal mobile gallery
		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
		System.out.println("Please select details manually");
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Albums']")).click(); // Albums
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='LendenTest']")).click(); // Lenden																													
		BorrowerRegistration.driver.findElement(By.xpath(
				"(//android.widget.FrameLayout[@content-desc=\"Photo, 29 November 2021 16 o'clock\"])[2]/android.widget.FrameLayout/android.widget.TextView"))
		.click(); // Select front Pic
		}catch(Exception ex) {}
		time.pageWaitForTime(10);
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView"))
		.click(); // Select Back side image of adhaar card
		BorrowerRegistration.driver.findElement(By.id("android:id/button2")).click(); // Select Gallery option
		// Click Personal mobile gallery
		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Albums']")).click(); // Albums
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='LendenTest']")).click(); // Lenden
		BorrowerRegistration.driver.findElement(By.xpath(
				"(//android.widget.FrameLayout[@content-desc=\"Photo, 29 November 2021 16 o'clock\"])[1]/android.widget.FrameLayout/android.widget.TextView"))
		.click(); // Select Back side of Aadhar card
		}catch (Exception e) {
		}
		time.pageWaitForTime(20);
		time.pageScroll("SUBMIT"); // Scroll bar to scroll till Submit button
		try {
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Eg. Mumbai']"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 16));
		} catch (Exception e) {}
		// For hide keyboard
//		BorrowerRegistration.driver.hideKeyboard();
		// driver.findElement(By.xpath("//android.widget.TextView[@text='Yes, I stay
		// here']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Self-owned']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='With Family']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']")).click();
		time.pageWaitForTime(15);
	}
}
