package com.lendenclub.test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import com.lendenclub.supporting.classes.WaitForTime;
import com.lendenclub.test.main.BorrowerRegistration;

public class SubmitBankStatement {
	WaitForTime time = new WaitForTime();

	public void page008SubmitBankStatement() {

		try {
			BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='Upload PDF Statement']")).click();
		} catch (Exception e) {
			System.out.println("Message is : " + e.getMessage());
			System.out.println("Cause is : " + e.getCause());
		}
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		time.pageScroll("Try Internet Banking"); // Scroll bar to scroll till Submit button
		// Scroll bar to scroll till Submit button
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='CLICK HERE TO UPLOAD 90 DAYS BANK STATEMENT']"))
		.click();
//		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
//		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='7488752498.pdf']")).click();
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='YES, UPLOAD']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.EditText[@text='Enter Password (if any)']"))
		.sendKeys("7488752498");
		// To minimize the keyboard
		BorrowerRegistration.driver.hideKeyboard();
		//		driver.findElement(By.xpath("//android.widget.TextView[@text='Of every month']")).click();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='UPLOAD STATEMENT']")).click();
		time.pageWaitForTime(5);
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']")).click();
		time.pageWaitForTime(30);
		// Confirm bank details
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		try {
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys("02014457596969"); // Account number
		} catch (Exception e) { }
//		try {
//			BorrowerRegistration.driver.findElement(By.xpath(
//					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText"))
//			.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 20)); // Account number
//			BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//			} catch (Exception e) { }
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		try {
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys("02014457596969"); // Account number
		} catch (Exception e) {	}
//		try {
//			BorrowerRegistration.driver.findElement(By.xpath(
//					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.EditText"))
//			.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 21)); // Account number
//			} catch (Exception e) {	}
		
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 17)); // IFSC code
		
		BorrowerRegistration.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup/android.widget.EditText"))
		.clear();
		BorrowerRegistration.driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup/android.widget.EditText"))
		.sendKeys(ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), 18)); // Account Holder name
		BorrowerRegistration.driver.hideKeyboard();
		BorrowerRegistration.driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']")).click();
		time.pageWaitForTime(8);
	}  }
