package com.lendenclub.supporting.classes;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.lendenclub.test.main.BorrowerRegistration;

public class TestListener implements ITestListener {

	@Override
	public void onTestStart(ITestResult iTestResult) {
	}

	@Override
	public void onTestSuccess(ITestResult iTestResult) {
	}

	@Override
	public void onTestFailure(ITestResult iTestResult) {
		
		try {
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) BorrowerRegistration.driver).getScreenshotAs(OutputType.FILE);
			// coppy screenshot file into screenshot folder.
			FileUtils.copyFile(file, new File(System.getProperty("user.dir") + "//Screenshot//" + fileName + ".jpg"));
			System.out.println("Screenshot is capatured");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Copy File from Source folder to Destination folder
				File source = new File("./Screenshot");

				File dest = new File("C:\\Sandeep\\Software\\Screen_Shots\\Borrower-Registration-Happy_Path");
				try {
					FileUtils.copyDirectory(source, dest);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				//Multiple Screenshots reduced to 1 Source folder 
				int no_of_files = source.listFiles().length;
				System.out.println("no_of_files:"+no_of_files);
				
				for(int i=0; i< no_of_files - 1; i++)
				{
					File file = source.listFiles()[i];
					System.out.println(file.getAbsolutePath());
					file.delete();
				}				
	}

	@Override
	public void onTestSkipped(ITestResult iTestResult) {
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
	}

	@Override
	public void onStart(ITestContext iTestContext) {
	}

	@Override
	public void onFinish(ITestContext iTestContext) {
	}
}

